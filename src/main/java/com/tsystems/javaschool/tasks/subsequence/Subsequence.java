package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null) {
        throw new IllegalArgumentException("Argument can't be null");  
        } else {
        
               
        int IndexJ = 0;
        boolean b = false;     
        
        int ListSizeX = x.size();
        int ListSizeY = y.size();
               
        
        if (ListSizeX > ListSizeY) {
            return false;
        }
        else if (ListSizeX == 0) {
            return true;
        }

        for (int i = 0; i < ListSizeX; i++) {

            b = false;

            for (int j = IndexJ; j < ListSizeY; j++) {
                    if (x.get(i).equals(y.get(j))) {
                        IndexJ = j++;
                        b = true;
                        break;
                    }
                }

                if (!b)
                {
                    break;
                }
            }

         return b;
         }

    }
}
