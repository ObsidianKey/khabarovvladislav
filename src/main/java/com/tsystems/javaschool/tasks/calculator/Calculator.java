package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */


    private List<Character> Operators = Arrays.asList('+', '-', '*', '/');


    public String evaluate(String statement) {

        if (statement == null || statement.length() < 3) {
            return null;
        }
        StringBuilder fullStatement = new StringBuilder((statement));
        StringBuilder result = processStatement(fullStatement);
        if(result == null){
            return null;
        }
        double res = Double.parseDouble(result.toString());
        if(Double.isInfinite(res) || Double.isNaN(res)){
            return null;
        }
        if(res % 1 == 0){
            return String.valueOf((int)res);
        }
        if((res * 10000) % 1 == 0){
            return String.valueOf(res);
        }
        return String.format("%.4f", res);
    }

    private StringBuilder processStatement(StringBuilder entry) {

        if(entry.length() == 0){
            return null;
        }
        Deque<Character> lastBracket = new ArrayDeque<>();
        List<Double> numbers = new ArrayList<>();
        List<Character> lastOperators = new ArrayList<>();
        int lastExpressionIndex = -1;
        
        //correct statement
        for (int i = 0; i < entry.length(); i++) {
            char symbol = entry.charAt(i);
            try{
                if((symbol == '.' && (!Character.isDigit(entry.charAt(i - 1)) || !Character.isDigit(entry.charAt(i + 1)))) ||
                        (Operators.contains(symbol) && ( (!Character.isDigit(entry.charAt(i - 1)) && entry.charAt(i - 1) != ')') ||
                                (!Character.isDigit(entry.charAt(i + 1)) && entry.charAt(i + 1) != '(') ))){
                    return null;
                }
                if(symbol == '('){
                    if(i == 0)
                    {
                        if(entry.charAt(i + 1) != '(' && !Character.isDigit(entry.charAt(i + 1))){
                            return null;
                        }
                    }
                    else if((entry.charAt(i - 1) != '(' && !Operators.contains(entry.charAt(i - 1))) ||
                            (entry.charAt(i + 1) != '(' && !Character.isDigit(entry.charAt(i + 1)))){
                        return null;
                    }
                }
                if(symbol == ')'){
                    if(i == entry.length() - 1){
                        if(entry.charAt(i - 1) != ')' && !Character.isDigit(entry.charAt(i - 1))){
                            return null;
                        }
                    }
                    else if((entry.charAt(i + 1) != ')' && !Operators.contains(entry.charAt(i + 1))) ||
                            (entry.charAt(i - 1) != ')' && !Character.isDigit(entry.charAt(i - 1)))){
                        return null;
                    }
                }
            }
            catch(IndexOutOfBoundsException e) {
                return null;
            }
            
            

            if (symbol == '(') { //check brackets
                lastBracket.addFirst(symbol);
            } else if (symbol == ')') {
                if (lastBracket.peekFirst() != '(') {
                    return null;
                }
                lastBracket.pollFirst();
            }

            if((Operators.contains(symbol) || i == entry.length() - 1) && lastBracket.size() == 0){
                StringBuilder expression;
                if(i == entry.length() - 1){
                    expression = new StringBuilder(entry.subSequence(lastExpressionIndex + 1, i + 1));
                }
                else{
                    expression = new StringBuilder(entry.subSequence(lastExpressionIndex + 1, i));
                }
                lastExpressionIndex = i;
                double number;
                
                // statement in brackets check
                boolean hasBrackets = false;
                char[] chars = expression.toString().toCharArray();
                for (char element : chars) {
                    if(element == '(' || element == ')'){
                        hasBrackets = true;
                        break;
                    }
                }
                try{
                    if(hasBrackets){
                        expression.delete(0, 1);
                        expression.delete(expression.length() - 1, expression.length());
                        number = Double.parseDouble(processStatement(expression).toString());
                    }
                    else{
                        number = Double.parseDouble(expression.toString());
                    }
                }
                catch(NumberFormatException e){
                    return null;
                }
                if(lastOperators.isEmpty() || (lastOperators.get(lastOperators.size() - 1) == Operators.get(0) ||
                        lastOperators.get(lastOperators.size() - 1) == Operators.get(1))){
                    if(i != entry.length() - 1){
                        lastOperators.add(symbol);
                    }
                    numbers.add(number);
                }
                else{
                    numbers.set(numbers.size() - 1, count(numbers.get(numbers.size() - 1),
                            number, lastOperators.get(lastOperators.size() - 1)));
                    lastOperators.remove(lastOperators.size() - 1);
                    if(i != entry.length() - 1){
                        lastOperators.add(symbol);
                    }
                }
            }
        }
        if(lastBracket.size() != 0){
            return null;
        }
        double result = numbers.get(0);
        for (int i = 0; i < lastOperators.size(); i++) {
            result = count(result, numbers.get(i + 1), lastOperators.get(i));
        }
        return new StringBuilder(String.valueOf(result));

    }



    private double count(double x, double y, char operator){
        switch (operator){
            case '+':
                return x + y;

            case '-':
                return x - y;

            case '*':
                return x * y;

            case '/':
                return  x / y;

            default:
                throw new IllegalArgumentException("Wrong operator. How did that even happen?");
        }
    }
}
